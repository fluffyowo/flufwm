# flufwm (gotham)
![2023-07-12-220725_1920x1080_scrot](https://github.com/Fluffyzwz/flufwm/assets/85907829/53925475-63d9-4290-863f-60608d18e8aa)
![2023-07-12-223619_1920x1080_scrot](https://github.com/Fluffyzwz/flufwm/assets/85907829/9be0cf62-9a1f-4e48-a21a-9712f265c738)

# flufwm (dracula)
![2023-07-13-183433_1920x1080_scrot](https://github.com/Fluffyzwz/flufwm/assets/85907829/41fce62f-3e86-4e86-b54c-e3fd8ca1fa67)
![2023-07-13-183431_1920x1080_scrot](https://github.com/Fluffyzwz/flufwm/assets/85907829/ca2846a5-cf3b-4057-b8a9-4141ee837366)


# flufwm (nord)
![2023-07-13-183715_1920x1080_scrot](https://github.com/Fluffyzwz/flufwm/assets/85907829/62815546-5b8b-48e1-b04a-88e0551f8b2d)
![2023-07-13-183709_1920x1080_scrot](https://github.com/Fluffyzwz/flufwm/assets/85907829/d8d5b9ef-b6dd-416b-8dd4-68bbdfad2c6e)

# flufwm (onedark)
![2023-07-13-184024_1920x1080_scrot](https://github.com/Fluffyzwz/flufwm/assets/85907829/612df1b8-825d-4f1d-ac99-bd59847f3f45)
![2023-07-13-184022_1920x1080_scrot](https://github.com/Fluffyzwz/flufwm/assets/85907829/3808d051-10fe-4be3-80ca-fc4d4dbeb2ad)
