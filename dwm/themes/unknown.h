static const char col_back[] = "#121111";
static const char col_gray1[] = "#212126";
static const char col_gray2[] = "#444444";
static const char col_gray3[] = "#bbbbbb";
static const char col_gray4[] = "#dbdfdf";
static const char col_cyan[] = "#8cb5af";
